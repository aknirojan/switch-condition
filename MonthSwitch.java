
public class MonthSwitch {
		public static void main(String[] args) {
			int num = 13;
			switch (num) {
			case 1:
				System.out.println("January\t31Days");
				break;
			case 2:
				System.out.println("February\t28Days");
				break;
			case 3:
				System.out.println("March\t30Days");
				break;
			case 4:
				System.out.println("April\t31Days");
				break;
			case 5:
				System.out.println("May\t30Days");
				break;
			case 6:
				System.out.println("June\t31Days");
				break;
			case 7:
				System.out.println("July\t30Days");
				break;
			case 8:
				System.out.println("August\t31Days");
				break;
			case 9:
				System.out.println("September\t30Days");
				break;
			case 10:
				System.out.println("October\t31Days");
				break;
			case 11:
				System.out.println("November\t30Days");
				break;
			case 12:
				System.out.println("December\t31Days");
				break;
			default:
				System.out.println("Invalid Month");
			}
		}
	}


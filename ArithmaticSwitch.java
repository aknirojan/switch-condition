import java.util.Scanner;
public class ArithmaticSwitch {
		public static void main(String[] args) {
			double number1;
			double number2;
			Scanner scan = new Scanner(System.in);
			{
				System.out.println("Enter the number1:");
				number1 = scan.nextDouble();
				
				System.out.println("Enter the number2:");
				number2 = scan.nextDouble();
				
				System.out.println("Enter the Operation:");
			    String op = scan.next();   
				switch(op) {
				case "A" : 
					System.out.println(number1 + number2);
					break;
				case "S" : 
					System.out.println(number1-number2);
					break;
				case "M" : 
					System.out.println(number1*number2);
					break;
				case "D" : 
					System.out.println(number1/number2);
					break;		
				}
			}
		}
	}
					
